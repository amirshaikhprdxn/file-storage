const item = document.querySelector('#item-name');
const quant = document.querySelector('#quant');
const save = document.querySelector('.save');
const name = document.querySelector('.name');
const value = document.querySelector('.value');
const clear = document.querySelector('.clear');
const modify = document.querySelector('.modify');
const dele = document.querySelector('.remove');

save.addEventListener('click',(event) => {
  event.preventDefault();
  addItem();
});

let addItem = () => {

  if (typeof (Storage) !== "undefined") {
    storeItem('item',item.value)
    storeItem('quantity',quant.value)

    listMaker(item.value, name, 'i-name');
    listMaker(quant.value, value, 'count');
    listMaker('update', modify, 'mod');
    listMaker('delete', dele, 'delete');
  }
  else {
    alert("Sorry, your browser does not support web storage...");
  }
}

let storeItem = (key, value) => {
  var existing = localStorage.getItem(key);
  existing = existing ? existing.split(',') : [];
  existing.push(value);
  localStorage.setItem(key, existing.toString());
}

let listMaker = (val, parent, cl) => {
  let i_name = document.createElement('li');
  i_name.innerHTML = val;
  i_name.setAttribute('class', cl);
  parent.appendChild(i_name)
}

clear.addEventListener('click', () => {
  localStorage.clear();
  location.reload();
});